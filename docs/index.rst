.. MAS507 documentation master file, created by
   sphinx-quickstart on Tue Aug 25 21:04:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. image:: ./src/figures/uia.png
   :width: 600px
   :align: center
   :alt: alternate text

|

.. raw:: html

   <h2 style="text-align: center">University of Agder</h2>
   <p style="text-align: center">Department of Engineering Sciences<br>Grimstad, Norway</p>
   
|

.. image:: ./src/figures/forside1.png
   :width: 12cm
   :align: center

|
|

MAS507 - Product Development and Project Management
===================================================

Student Group
-------------

- Sindre Bokneberg

- Even Falkenberg Langås

- Jørgen Fone Pedersen


Lecturers
---------

- Sondre Sanden Tørdal, PhD
   - Postdoctoral Fellow in Mehcatronics
   - https://www.uia.no/kk/profil/sondrest

- Mette Mo Jakobsen, PhD
   - Professor in Product Development and Project-based learning
   - https://www.uia.no/kk/profil/mettemj


.. toctree::
   :numbered: 2
   :maxdepth: 2
   :caption: Contents:

   src/introduction
   src/Jorgen
   src/Product_development
   src/sindre
   src/even
   src/camera
   src/results
   src/discussion
   src/conclusion
   src/z_bibliography
   src/appendix

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
