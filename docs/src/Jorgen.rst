===================
Project Management
===================

This chapter contains some information on how the project has been planned and managed during its course and on some of
the new tools that have been introduced and used in this course. A team agreement has also been made, this is to ensure 
that all team members have the same expectations and demands to the project, team-work and eachother. This agreement 
can be viewed in the :ref:`appendix <appendix>`.


.. contents::


Trello Board and SCRUM
=======================

For the planning of this project, some common project planning methods have been used to keep track
of the project's progress and plan for upcoming tasks. The main part of the planning has been done using a 
Trello board as a way to implement SCRUM into our project. SCRUM is often used for software development
because it is a simple framework for effective collaboration. One major advantage of SCRUM using Trello is that it is easy for all project members to be active in project planning. The SCRUM team consists of
the development team members and a SCRUM master. The SCRUM master is the one in charge of keeping the Trello Board up to
date and is responsible for promoting and supporting the use of SCRUM as the main project planning tool. 
The development team members are the persons that perform the tasks defined in the Trello board. For a small 
group such as for this project, all three members of the group are development team members, while one of the 
members has the additional task of being SCRUM master. The group has used a version of  SCRUM for 
defining tasks and assigning resources. The SCRUM process is to divide all tasks into their respective lists. 
:numref:`Trello` shows a screenshot of the Trello board used in the project. :cite:`scrum`.

+ Suggested Tasks
+ Approved Tasks
+ In Progress
+ In Review
+ Blocked
+ Complete 

As all members of the group have access to the Trello board, every group member can add tasks to the suggested
tasks list. All tasks that are appended to this list will then be reviewed by the group during the group meetings
if all members agree on the task the SCRUM master may place the tasks in the approved list. The team
member in charge of performing the task should also be assigned to the task before adding it to the approved list. 
The tasks have been color labeled to differentiate the tasks from their domain. The different colors related to the 
different domains are shown below.

+ Violet: Report.
+ Blue: Mechanical.
+ Red: Video.
+ Orange: Software
+ Yellow: Project planning

Tasks in the approved list are the tasks to be performed, they may have a deadline and should always be assigned 
either one or several team members. When the task starts, the task shall be moved to the in-progress list. When it 
is complete, it shall be moved to the in review list. The in review list is meant to be a final check before completing
a task. All members should look over the result of the task and approve it, if there are any objections to the 
result from the task it shall be stated during this time. Any proposed modifications or changes to the task result should 
also be performed here. When all members are agreed upon the result the task can finally be moved to the 
complete list. 

The blocked list is only used if there is a task that cannot be performed during its intended period due to 
unexpected events that may occur. For example, if the task is to 3D print a part during that day and the printers are occupied, 
the task may be moved to the blocked list before it is rescheduled.

.. figure:: ./figures/Trello2.png
   :name: Trello
   :width: 1000px
   :align: center

   Screenshot of the Trello board used in the project with SCRUM. 

GIT
====

GIT has been used actively in this project for enabling multiple members to work on the same software project.
Using GIT each team member can edit their own version of the source code locally before merging it together. 
To start using GIT, a repository has to be created. Using a DevOPS platform to create this repository, like gitlab
or github, enables the main repository to be stored on an online server. This makes it easy to download the
main repository from any location as long as there is a internet connection available. Each teammember can 
then create their own branch, download it on their personal computer and modify any element of the source 
code in the repository. As long as the user works on their own branch this does not affect the main source code
before the branch is merged with the master branch. The master branch is the main branch in which all source code
should be fully functional and working. To ensure that all source code that is merged to master is functional, 
a person is assigned to be the merge master. The merge master is the person responsible to merge the code for all 
users. 

.. figure:: ./figures/Git-Logo-2Color.png
   :name: GIT
   :width: 400px
   :align: center

Documentation using SPINHX
===========================

Sphinx is a tool for creating documentation and is particularly used for creating python documentation. Sphinx enables the use of 
multiple text files in either reStructuredText format or markdown to be translated into various output formats, 
automatically producing cross-references, indices, etc. Sphinx can also output into other formats such as LaTeX, ePUB, Texinfo, manual pages,
and plain text. :cite:`sphinx`. Sphinx has been used in this project to document the process as a final report in the subject.


.. figure:: ./figures/sphinxheader.png
   :name: sphinx
   :width: 400px
   :align: center

 

