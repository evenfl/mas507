===================
Product development
===================

In the process of developing a product, there are a lot of factors to consider to get the best possible solution. 
In this chapter, some of these considerations are discussed for a strawberry picking robot. In this chapter it is considered that 
the robot will be a fully functioning strawberry-picking robot that will be used in real-life applications. 


.. contents::

Market needs
=============
For a product to be able to produce a profit in the market, it is necessary to design the product after what 
the market wants. The market may however be very different depending on what sort of product is being designed. 
Firstly it is therefore important to find the correct market for the product. 

For a strawberry picking robot, the market will be for producers of strawberries. Within this group, the 
market will consist of large scale producers, as for small scale producers the cost of autonomizing the
picking process is most likely not financially viable. Thus, this is a very niche market, and for niche markets 
a product should have a high level of profit to be financially sustainable. However, if there is a 
need in this market there is a chance of possibly fulfilling this need to create a sustainable business. 
To find the need for large scale producers of strawberries, a possible solution is to find a large scale strawberry
producer and form a partnership. This way the partner may provide feedback to the development as it is happening
and testing may happen in situ. The partner may also be providing financial support during development for a
reduction in the price of the final product. This may be the best solution for creating a product for such a niche market.

Maintenance and support
-------------------------
On a product that has a relatively long lifetime and cost, maintenance and support are usually something that the seller will offer. 
This gives the customer some relief from unexpected costs due to errors that may occur with the product 
during its lifetime.

For a strawberry picking robot, a maintenance and support agreement is required to be able to compete on the market. 
For such a specialized product, should anything go wrong with it, it will require specialized personnel to fix. 
Thus there needs to exist a service for this at the producer with personnel that can perform maintenance in situ. 
For strawberry producers, losing a robot due to faults may be very costly if the producers rely heavily on the robot. 
Thus it is important to be able to provide support as fast as possible to keep the customers happy. 

Product Safety
---------------

Safety is always a key factor when designing a product. To ensure the safety of the product, copious testing
during various conditions should be made and documented. Depending on the product, various international safety
standards are available for products to earn different safety certifications. This ensures a common practice
amongst different producers, which gives assurance to the safety of products by consumers. Safety refers
to in this manner both the environmental safety and the personal safety of people or animals. Environmental
safety is the assurance that the product will not harm or otherwise cause negative effects on its imminent
environment. Figure :numref:`ce-mark` shows a common safety standard that is used on products in the EU. 
By affixing the CE marking to a product, a manufacturer declares that the product meets all the legal requirements
for CE marking and can be sold throughout the European Economic Area (EEA) :cite:`CE-marking`.


.. figure:: ./figures/ce-mark.jpg
   :name: ce-mark
   :width: 250px
   :align: right
   
   *CE marking*

For the strawberry picking robot concept of choice, the robot has a low risk of causing any issues that will
violate the safety of personnel or the environment. The robot is small and its actuators are very low in torque, 
any objects obstructing its path would not take damage. As the robot is powered by a battery however, 
there is always the risk of a short circuit. This may cause sparks and possibly a fire. For a final concept,
this should be taken into consideration by waterproofing the robot, and hardwiring and potting wires. 
The environmental aspect of safety has no large risks with the current concept, the robot is gentle on the 
berry bushes and has no leakage of dangerous fluids. 


Aestethic design
-----------------
An aesthetic design is important for almost all products, through its design a product can signalize its strengths in a competitive market. 
The aesthetic design of a product can be used to describe: :cite:`Produktutvikling`

+ A products function, purpose, abilites, etc.
+ A products qualities
+ how the user should react
+ the producer, brand, etc. 

For the strawberry picking robot, this aspect is thus something that should be used to the product's advantage. By producing the
chassis and picking assembly in metal, the robot will have a more robust look. Also by hiding wires and electronics, the robot
will have a much more clean look that will signalize that this is a quality product. The producer's logo should also be painted 
on the chassis, this will generate interest in the company for anyone that sees the robot in action. 


Environmental impact
====================
Sustainable development has become of inceasingly large importance for new product developments in the
last years. The negative impact the product may have on the enviroment and climate is important to minimize as much as possible. 
This is important to consider for the entire lifetime of the product, and should be planned in detail in the development phase.  

Production
-----------
In the case of eventual mass production, sustainable production needs to be a consideration. Each part that is used in the final product
has its own environmental impact. For a strawberry picking robot the production will most likely not be on a very large scale, but more 
production after order. However, a lot of the components used in the robot are mass-produced, such as the printed circuit boards (PCBs), wires, 
and batteries, etc. When choosing manufacturers of these components, the sustainability of their production should be evaluated, both 
environmentally and socially. With social sustainability, it is meant that the manufacturers pay their workers a fair wage, that they have
decent working conditions, and so on. Choosing a manufacturer that focuses on sustainability in production might cause the overall price 
to be increased in the start. But as the world is increasingly working towards sustainable solutions the investment into these manufacturers 
could be worth it. 


Product lifecycle
------------------
The lifecycle of a product starts with the design process, and usually then ends when the product is discarded. However, that does not
have to be the end. Circular economy is a concept that takes into the account recycling of the products as well, and the use these 
recycled materials to produce new products that can start the lifecycle anew. To enable this to be possible, it should be 
taken into consideration from the very start of the lifecycle in the design process. 
By taking this into consideration already in the design process for new products, the enabling of circular economy will be much easier
in the years to come. 

For a strawberry picking robot, this will include choosing easily recyclable materials. 
To ensure that all materials are recycled when the robot is decommissioned, it should be returned to the producers such that they can dismantle the robot. Then all robot components that are still usable may be used again immediately, effectively shorting down the product chain for new
products. For the prototype, 3D printed plastic was the best choice for the robot chassis and picking arm, but for the final product, metal would be a better choice.
Aluminium will be a great choice for this as it contains all the right material properties that are needed and it is infinitely recyclable.
Aluminium is commonly recycled and the infrastructure around this is well established so enabling the recycling of these parts should pose no
issues. When the robot is decommissioned it will be dismantled and the aluminium used will be sold as scrap metal if it is no longer usable in its current condition.  
The recycling of electronics on the robot is a much more cumbersome process as it is a highly complex part in terms of the composition of materials. This makes it not very profitable and is thus a process that relies much more on how the governments of
the world takes this problem seriously by introducing legislations and beneficial bonuses for recycling electronic waste. 
However, as the world's electronic waste is only increasing, the need for recycling will only be higher, and companies specializing in this is
started all over the world. By using one of these companies the electronic waste will be recycled. In :numref:`circ_economy` a figure
of the various stages of a circular economy is shown. :cite:`circular_economy`

.. figure:: ./figures/circular_economy_en.svg
   :name: circ_economy
   :width: 500px
   :align: center

   *Circular Economy*

UN goals for sustainable future
-------------------------------
In 2015 the United Nations set 17 goals that is intended to achieve a better and more sustainable future for all. It is intended 
to be achieved within 2030. Thus when designing a new product, these goals should be taken into consideration to ensure that 
the product will not cause these goals to be harder to reach. The different goals can be seen in figure :numref:`UN_goals` :cite:`UN_goals`



.. figure:: ./figures/UN_goals.png
   :name: UN_goals
   :width: 1000px
   :align: center

   *UN's 17 goals for a sustainable future*

Not all of these goals may apply to a strawberry picking robot, but some of them do. The goals that are considered affected by 
our product is listed below

+ 2: Zero Hunger
+ 9: Industry, Innovation and Infrastructure
+ 12: Responsible Consumption and Production

Starting with the goal of zero hunger, a strawberry-picking robot will not solve this problem.
However, it might introduce some effects that may contribute to this goal.
Increasing the efficiency in food production will decrease the price of food and in this way, a strawberry-picking robot may indirectly 
contribute towards less hunger. Even though strawberries are not exactly the food to stop the world from starving, the introduction of 
such a robot may cause more publicity on the effects of autonomizing food production and thus cause more money and workforce to be 
invested in similar products. This may contribute to a more effective food production over time and thus help towards 
the zero hunger goal. 

Creating a robot that can autonomously pick strawberries is an innovative concept, and such innovation may lead to an increase in interest 
in the autonomous food production industry. More interest in this industry will cause more investments in this
sector and will thus increase the industrialization of food production overall. The more the food production industry is effective
the more gain it will yield which in turn gives a large need for infrastructure to be able to move all the products as efficiently as possible. 

By doing this analysis, as is being done here in the product development phase, responsible consumption and production are taken 
into account already from the start. By designing the product such that responsible consumption and production is viable and easy from 
the start, the processes needed to perform this will be much easier when the time comes. 