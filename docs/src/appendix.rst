Appendix
========

.. _appendix:

Team agreement
--------------

.. figure:: ./figures/teamAgreement1.png
   :name: team1
   
.. image:: ./figures/teamAgreement2.png
   
.. image:: ./figures/teamAgreement3.png

Python files
------------

All python files can be found `here <https://gitlab.com/evenfl/mas507>`_. The most relevant files are shown below.

MainController.py
"""""""""""""""""

.. literalinclude:: ../../src/MainController.py
   :language: python
   :linenos:

StrawberryDetector.py
"""""""""""""""""""""

.. literalinclude:: ../../src/StrawberryDetector.py
   :language: python
   :linenos:

JetbotController.py
"""""""""""""""""""

.. literalinclude:: ../../src/JetbotController.py
   :language: python
   :linenos:
