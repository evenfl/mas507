Computer vision
===============

For the robot to be able to know where to move and where to pick strawberries, computer vision was implemented by the help of OpenCV. 
A camera connected to the Jetbot was used to detect strawberries and aruco markers.


.. contents::

Camera calibration
------------------

In the beginning the camera was calibrated to make sure that the algorithm could be able to make accurate detections and measurements.
:numref:`calibrationCheck` shows one out of 20 images that was taken and used to calibrate the camera.

.. figure:: ./figures/calibrateCheckerboard.png
    :name: calibrationCheck
    :align: center
    :width: 8cm

    *One of the images used for calibration*

The results from the calibration can be seen in :numref:`calibrationResults`.

.. figure:: ./figures/rawVsCalibrated.png
    :name: calibrationResults
    :align: center
    :width: 20cm

    *From the left: Raw and calibrated image*

Aruco marker detection
----------------------

The aruco markers was detected by using the code provided by the supervisor and the calculated x-, y- and z-position 
as well as the id of the aruco marker was published to be able to access it in other ROS nodes.


Subscribers and publishers were used to transmit variables between different python programs. 
The code below shows how the Jetbot sends the aruco marker position from JetbotCamera.py to MainController.py. JetbotCamera.py had to include three publishers, 
and MainController.py had to include three subscribers. The publishers were initialized with the following code:

.. code-block:: python
    
    # Publisher
    pubx = rospy.Publisher('x', Float32, queue_size=1)
    puby = rospy.Publisher('y', Float32, queue_size=1)
    pubz = rospy.Publisher('z', Float32, queue_size=1)

Later in the code, the coordinates were sent using the code below where tvecs is an array including x-, y- and z-coordinates of the aruco marker. 
The code to achieve these coordinates was provided by the supervisor.


.. code-block:: python

    pubx.publish(tvecs[i][0,0])
    puby.publish(tvecs[i][0,1])
    pubz.publish(tvecs[i][0,2])

The subscribers where initialized with the following code:


.. code-block:: python

    # Subscriber
    arucoXSub=rospy.Subscriber('x', Float32, callbackx)
    arucoYSub=rospy.Subscriber('y', Float32, callbacky)
    arucoZSub=rospy.Subscriber('z', Float32, callbackz)

The callback functions refer to the functions below that sets arucoX, arucoY and arucoZ to be equal to the received value.

.. code-block:: python

    # Create callback to handle data from subscriber
    def callbackx(msg):
        global arucoX
        if msg.data:
            arucoX = msg.data
    def callbacky(msg):
        global arucoY
        if msg.data:
            arucoY = msg.data
    def callbackz(msg):
        global arucoZ
        if msg.data:
            arucoZ = msg.data
    def callbackid(msg):
        global arucoID
        if msg.data:
            arucoID = msg.data


No matter how good the detection algorithm is, it could sometimes be held back by the camera. The camera struggled with moving objects because of the slow shutter speed. 
An example was when turning to look for an aruco marker. The images was too blurry for the algorithm to be able to detect the aruco marker.
Therefore, the turning sequence when looking for an aruco marker was changed to chopped turning where the robot turned for a quarter of a second and then paused for a quarter of a second.
This solution fixed the issue, and the algorithm could now detect the aruco markers when turning.


Strawberry detection
--------------------

The strawberry detection code provided by the supervisor where built up to only detect the largest red contour in the image.
If the robot was far enough away to be able to see multiple strawberries, the robot struggled to decide which one to choose.
A solution to this problem was to have the robot detect all strawberries and choose the one to the right instead of choosing the one with the largest contour.
With this approach, the robot would always pick the strawberries in the correct order. In addition, it would not struggle to know which one is longest to the right.
On the other hand, this would make the robot see all contours passing through the threshold as a strawberry, even if it is just one pixel of red.
A threshold was therefore added to the radius of the circle drawn around the strawberry to be within 12 and 70 pixels.
When the robot is picking strawberries, the berries will never go outside this threshold, but it will weed out all contours larger or smaller, which is very beneficial.
The solution above was implemented in :eq:`StrawberryDetector.py` by replacing the function :eq:`find_biggest_contour` with a modified version of which was called :eq:`find_red_contours`. (See the code below)

.. literalinclude:: ../../src/StrawberryDetector.py
    :pyobject: StrawberryDetector.find_red_contours

When all contours within the color range was detected, they were sorted out by the if statement below. 
This if statement filtered out all detections with a radius outside the range of a possible strawberry.
In addition, it only saved the position if the detected contour is longer to the right than the previous detected strawberry.

.. code-block:: python

    # Return detection results to class
    if (tvec[0] > self.x or self.x == 0.0) and tvec[2] > 0.02 and r < 70 and r > 12:
        self.x = tvec[0]
        self.y = tvec[1]*(-1) - 0.07
        self.z = tvec[2]*0.9


The rest of the code can be seen in the appendices. These modifications resulted in the images shown in :numref:`beforeAndAfterModifications`.

.. figure:: ./figures/strawberryDetectionBeforeAfter.jpg
    :name: beforeAndAfterModifications
    :align: center
    :width: 20cm

    *Before and after modifications to the strawberry detection algorithm*


Another problem that was encountered was the surroundings. In the testing facilities, there were some brick walls that the robot detected as strawberries. 
By changing the threshold values set in :eq:`StrawberryDetector.py`, the result in :numref:`beforeAndAfterColorModifications` was accomplished. 
The only parameters that was necessary to tune to achieve these results was the lower saturation values.

.. image:: ./figures/newTreshold.png
    :align: center
    :width: 6cm

.. figure:: ./figures/strawberryDetectionColor.jpg
    :name: beforeAndAfterColorModifications
    :align: center
    :width: 20cm

    *Before and after modifications to the color threshold in the strawberry detection algorithm*

Lighting had a significant impact on the strawberry detection. 
If the strawberry was in a darker environment, for example caused by shadows, the berry was perceived with a slightly smaller radius, and therefore the accuracy of the robot arm decreased.
This shows the importance of good lighting conditions when working with computer vision. The final testing was done a place where the lighting was consistent for all the strawberries.

When the camera was calibrated and the program were built up, the testing could begin. The results from the testing is explained in the next chapter.