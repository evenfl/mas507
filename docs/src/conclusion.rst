Conclusion
==========

A strawberry picking robot was made and it passed the test in the predefined artificial strawberry field.
The software proved to be reliable with a finite-state machine controlling the program flow. 
In addition, the picking sequence was consistent thanks to the tailored vision algorithm.

A mechanical concept was made, including 3 servo motors, a frame, a robot arm with two revolute joints and a basket to store the berries.
The mechanical system were all 3D printed and the process with concept phase, design, production and verification worked quick and seamlessly. With the use of a modular design with interchangeable parts redesign and adjustments 
were performed while avoiding changing the belonging parts. By only changing the applicable parts much time was spared. 