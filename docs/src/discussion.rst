Discussion
==========

The results showed a consistent accuracy when picking strawberries, as well as a reliable software capable of performing the task at hand.
It can be seen that the robot sometimes tilted when picking a strawberry. 
That happened because some of the strawberries was attatched with tape and therefore it held on to the bush with a greater force than the berries attatched with a magnet.


Product development
-------------------

From a product development point of view, it is highly relevant to look at the potential of a robot like this. 
The concept developed in this project has a great potential in a controlled environment, such as in industrialized farming.
However, it can not be considered as an option for a harsh norwegian farming environment with rough terrain and different weather conditions.
For the robot to be able to work in such conditions it would need larger wheels, a waterproof case, a bigger battery pack and a more versatile software and mechanical design capable of picking strawberries many different positions without 
destroying the strawberries nor the plant.

Another issue that has to be evaluated when picking real strawberries is to not damage the strawberry. 
The concept carried out in this project could possibly damage some of the strawberries because of the sharp edges on the gripper and the hard plastic.
However, increased precision could possibly reduce the number of damaged strawberries to an acceptable level.


Mechanical Design
----------------

Some key properties were given for the mechanical design, and the most important were to keep servos and joints to a minimum and have a modular design which were solid and easy to print. The robot arm assembly consist of 
2 joints, one main for the arm and one outer for the gripper. As some strawberries hang low, down to 2cm, an outer joint was nescessary. However, this also made it possible to lock the berries in place and make a more 
precise picking assembly. The only negative part is the extra weight from the outer joint servo and a slightly more complex control algorithm. 

Neither parts broke during testing, except for when testing with one servo at the main joint. In this case, the plastic casing on the servo cracked due to excessive wear and strain. 
However, the plastic casing was glued back together and used in the final trial and working as it should. 
This proved that one servo was not enough to lift the robot arm, especially when it got stuck in a strawberry bush, when the stress where at a maximum. 
This greatly reduced the quivering in the system and were no problem implementing besides the time it took to print the new main bracket.