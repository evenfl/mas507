.. role:: raw-latex(raw)
    :format: latex html

Software
========

.. image:: ./figures/logoer.png
    :width: 17cm

The Jetbot was programmed with python in a ROS environment. It ran on an Ubuntu OS installed on the Jetbot.
Object oriented programming was used actively with a class called Robot.
This class included different functions to turn left and right, stop, move forward, back up, set motor speeds and angles and to approach items.
An enumeration based state machine was used to have a clear way of developing and perceiving the code. 
This allowed for easier debugging as well as an easy way to understand the program flow.


.. contents::


Functional description
----------------------

A file called :eq:`MainController.py` contained the program that decided the behaviour of the robot. 
This main file will be explained in this section. The flow chart below shows the detailed program flow.

.. image:: ./figures/Flytskjema-Orientation_mode.png
    :scale: 80 %

.. image:: ./figures/Flytskjema-Picking_mode.png
    :scale: 80 %

Finite-state machine
""""""""""""""""""""

For the robot to perform the given task in a the order that the flow chart above shows, a finite-state machine was used. 
A state machine is an abstract machine that can be in only one of a finite number of states where a set of conditions can cause a transition to another state. :cite:`fsm`
This was used as a method to build up the software needed to make the robot perform the desired task of picking the red strawberries in the strawberry field.

In python, the most efficient way to make a state machine is by using enumerations. :numref:`stateMachineMain` shows the different states 
where a main state sets what the robot should do.
Both the pick berry state and the change side state has several substates. All the states and transitions are explained in this section.

.. figure:: ./figures/stateMachineMain.png
    :name: stateMachineMain
    :align: center
    :width: 10cm

    *Main states*

**Main states**

* :eq:`INIT` : Initializes the robot
* :eq:`PARK` : Stops the robot and sets the robot arm to a 45 degree angle. The robot searches for aruco marker 1 or 3 if none is detected.
* :eq:`MOVE_TO_NEXT_BERRY` : Moves towards the aruco marker
* :eq:`PICK_BERRY` : Picks berry and returns to the same position
* :eq:`CHANGE_SIDE` : Changes side to the other side of the strawberry field
* :eq:`FINISH` : Stops the robot

**Main states transitions**

0. When the :eq:`INIT` state is finished
1. If distance to aruco marker 1 is more than 50 cm
2. When the robot has moved forward so that the aruco marker is 15 cm closer
3. When :eq:`PICK_BERRY` state is finished
4. If distance to aruco marker 1 is less than 50 cm
5. When :eq:`CHANGE_SIDE` state is finished
6. If distance to aruco marker 3 is less than 45 cm

.. figure:: ./figures/stateMachinePickBerry.png
    :name: stateMachinePick
    :align: center
    :width: 18cm

    *States within the PICK_BERRY substate*

**Pick berry states**

* :eq:`INIT` : Initialize the :eq:`PICK_BERRY` state and transitions to the :eq:`FIND_BERRY` state  
* :eq:`FIND_BERRY`` : Turns 90 degrees to the right. Waits 0.5 seconds so the camera has time to find the berry.
* :eq:`APPROACH_BERRY` : The robot approaches the strawberry
* :eq:`POSITION_ROBOT_ARM` : Positions the robot arm by using inverse kinematics explained in Chapter 4.
* :eq:`MOVE_TO_PICKING_POSITION` : Moves forward a few cm to get to picking position
* :eq:`PICK` : Picks the strawberry by locking it with the gripper
* :eq:`BACK_UP` : Backs up a few cm
* :eq:`POSITION_BERRY` : Positions the berry above the basket
* :eq:`RELEASE_BERRY` : Releases the berry by opening he gripper
* :eq:`EXIT` : Turns 90 degrees to the left and goes into the :eq:`PARK` state

**Pick berry states transitions**

0. When :eq:`INIT` state is finished
1. When the robot has turned 90 degrees and a berry is detected
2. If distance to strawberry is less than :eq:`maxPickingDistance` and more than :eq:`minPickingDistance`
3. When the robot arm is positioned
4. When :eq:`MOVE_TO_PICKING_POSITION` state is done
5. When the gripper has locked the berry
6. When :eq:`BACK_UP` state is finished
7. When the robot arm is in position over the basket
8. When the gripper has released the berry
9. If no strawberry is detected in 0.5 seconds

.. figure:: ./figures/stateMachineChangeSide.png
    :name: stateMachineChange
    :align: center
    :width: 18cm

    *States within the CHANGE_SIDE substate*

**Change side states**

* :eq:`INIT` : Move forward 20 cm
* :eq:`TURN_RIGHT_1` : Turns right
* :eq:`MOVE_FORWARD` : Moves towards aruco marker 2
* :eq:`TURN_RIGHT_2` : Turns right
* :eq:`EXIT` : Moves forward approximately 10 cm and goes to :eq:`PARK`

**Change side states transitions**

0. When :eq:`INIT` state is finished
1. If aruco marker 2 is detected
2. If distance to aruco marker 2 is less than 25 cm
3. When the robot has turned approximately 90 degrees: :eq:`if t-t_0 > t_turnRight`

Program description
-------------------

The different robot operations can be executed by using the member functions of the :eq:`Robot` class.
This class includes the variables and functions in the table below. 
All the functions can be seen in the :ref:`appendices <appendix>`.

+-----------------------------------+
|          **class Robot**          |
+-----------+-----------------------+
|        Variables:                 |
|                                   | 
|                                   | 
|        - :eq:`L_0z`               | 
|                                   |
|        - :eq:`L_0y`               |
|                                   |
|        - :eq:`L_1`                |
|                                   |
|        - :eq:`L_2`                |
|                                   |
|        - :eq:`servoAngle0_Offset` |
|                                   |
|        - :eq:`servoAngle1_Offset` |
|                                   |
|        - :eq:`servoAngle0`        |
|                                   |
|        - :eq:`servoAngle1`        |
|                                   |
|        - :eq:`rightWheelSpeed`    |
|                                   | 
|        - :eq:`leftWheelSpeed`     |
+-----------------------------------+
|        Functions:                 |
|                                   |
|                                   |
|        - :eq:`setAngles()`        |
|                                   |
|        - :eq:`setServoMotors()`   |
|                                   | 
|        - :eq:`stop()`             |
|                                   |
|        - :eq:`turnRight()`        |
|                                   |
|        - :eq:`turnLeft()`         |
|                                   |
|        - :eq:`moveForward()`      |
|                                   |
|        - :eq:`backUp()`           |
|                                   |
|        - :eq:`followItem()`       |
|                                   |
|        - :eq:`moveRobotArm()`     |
+-----------------------------------+

In real life, the measurements taken from the camera can sometimes be inaccurate.
An example which happened in the beginning was that the Jetbot considered the aruco marker to be closer than it actually was in one measurement and therefore moved to the wrong state based on this single measurement.
Counters was therefore implemented to make sure that the Jetbot only accepted a series of 5 measurements that met the requirements.


Control system
--------------

Wheel servos
""""""""""""

The speed of the wheels were known to have a minimum and maximum value of 204 and 408 and were controlled by using the values in this scope directly. 
However, it is sometimes interesting to control the travel distance of the robot.
To be able to move the robot a given distance, the distance was tested for 10 seconds of forward motion with the function :eq:`moveForward()`. 
The result was 92 cm in 10 seconds which corresponds to 0.092m/s. However, the wheels uses some time to start, and in 1 second, the robot only moved 8 cm. 
1.2 seconds gave approximately 9.2 cm. Therefore, 0.2 seconds was assigned to the variable :eq:`t_start`. 
The function below was used to calculate the time to move a given distance:

.. literalinclude:: ../../src/MainController.py
    :pyobject: moveForwardTime

Different movements where executed by using the functions below.


.. literalinclude:: ../../src/MainController.py
    :pyobject: Robot.moveForward

    
.. literalinclude:: ../../src/MainController.py
    :pyobject: Robot.backUp

    
.. literalinclude:: ../../src/MainController.py
    :pyobject: Robot.turnLeft

    
.. literalinclude:: ../../src/MainController.py
    :pyobject: Robot.turnRight


Robot arm servos
""""""""""""""""

Unlike the servo motors running the wheels, the servo motors for the robot arm was locked to only 180 degrees of motion, and the input of these servos had to be an angle. 
A datasheet proved to be difficult to find, but by testing the inputs, the minimum and maximum value was found to be respectively 80 and 540. 
To simplify the input from a range of 80-540 to 0-180 degrees, the equation below was used:

.. code-block:: python

    servoSetpoints.servo1 = angle*(540-80)/180 + 80

A saturation was added to JetbotController.py:

.. code-block:: python

    for i in range(0, 5):
        if i <= 1:
            # Limit input to u in [204, 408]
            u[i] = max(204, u[i])
            u[i] = min(408, u[i])
        else:
            # Limit input to u in [80, 540]
            u[i] = max(80, u[i])
            u[i] = min(540, u[i])
        
        # Set PWM
        self.adafruit.set_pwm(i, 0, u[i])

It was desired to control the servo motors controlling the robot arm using angles in global coordinates to easily be able to set a position of the robot arm.
This was done in the function :eq:`setAngles` below. The variables :eq:`servoAngle0_Offset` and :eq:`servoAngle1_Offset` was set to the mechanical offset the motors had relative to the global coordinate system when given an input of 0 degrees.

.. literalinclude:: ../../src/MainController.py
    :pyobject: Robot.setAngles

The servomotors were set by using the function below.


.. literalinclude:: ../../src/MainController.py
    :pyobject: Robot.setServoMotors

Trajectory planning for the robot arm
"""""""""""""""""""""""""""""""""""""

When first testing the code, step inputs were used to control the 3D-printed robot arm. 
The step input made the movements very aggressive even causing the whole robot to tilt.
To smoothen out these movements, a 3rd order polynomial trajectory was used as input for the angular position of the robot arm and the gripper.
A step input causes an infinitely large spike in the velocity. A ramp input does the same, but for acceleration.
A 3rd order polynomial causes no spikes in velocity nor acceleration. 
However, jerk, which is the derivative of the acceleration gets an infinitely large spike when starting and stopping the robot arm.
In some applications, this spike can be preferable to fix to avoid vibrations when actuating the joints.
The strawberry picking robot, which is put together by several 3D-printed parts and loose electronics and cables is already vibrating enough that it would not make a significant improvement to make the polynomial a 4th order.
A 3rd order polynomial results in the behaviour seen in :numref:`smoothAngleChange`

.. figure:: ./figures/smoothAngleChange.jpg
    :name: smoothAngleChange
    :align: center
    :width: 12cm

    *Angular position, velocity and acceleration of a 90 degree change in angle. This graph shows how the input to the servo motors looks when changing the angle*


The polynomial for the angle and angular velocity is shown in the following equations:

.. math::
    \theta(t) = a_0 + a_1 \cdot t + a_2 \cdot t^2 + a_3 \cdot t^3

.. math::
    \dot\theta(t) = a_1 + 2\cdot a_2 \cdot t + 3\cdot a_3 \cdot t^2

When both the initial and the desired angle and angular velocity as well as the time period of the movement is known, then :raw-latex:`\(a_0, a_1, a_2, a_3\)` can be evaluated with the four equations below:

.. math::
    \theta(0) = a_0 = \theta_0

.. math::
    \dot\theta(0) = a_1 = \dot\theta_0

.. math::
    \theta(T) = a_0 + a_1 \cdot T + a_2 \cdot T^2 + a_3 \cdot T^3

.. math::
    \dot\theta(T) = a_1 + 2\cdot a_2 \cdot T + 3\cdot a_3 \cdot T^2

.. math::
    \begin{bmatrix}
        1 & 0 & 0 & 0 \\
        0 & 1 & 0 & 0 \\
        1 & T & T^2 & T^3 \\
        0 & 1 & 2\cdot T & 3\cdot T^2
    \end{bmatrix} \cdot
    \begin{bmatrix}
        a_0\\
        a_1\\
        a_2\\
        a_3
    \end{bmatrix} = 
    \begin{bmatrix}
        \theta_0\\
        \dot\theta_0\\
        \theta_1\\
        \dot\theta_1
    \end{bmatrix}

    
.. math::
    \begin{bmatrix}
        a_0\\
        a_1\\
        a_2\\
        a_3
    \end{bmatrix} =     
    \begin{bmatrix}
        1 & 0 & 0 & 0 \\
        0 & 1 & 0 & 0 \\
        1 & T & T^2 & T^3 \\
        0 & 1 & 2\cdot T & 3\cdot T^2
    \end{bmatrix}^{-1} \cdot
    \begin{bmatrix}
        \theta_0\\
        \dot\theta_0\\
        \theta_1\\
        \dot\theta_1
    \end{bmatrix}


This was implemented in python using the following code:

.. literalinclude:: ../../src/MainController.py
    :pyobject: Robot.moveRobotArm


Path planning
^^^^^^^^^^^^^

Since the strawberry field is known and there is no obstacles in the field, it was decided to go with a straight line whenever the robot is approaching an object. 

A function was made to follow a set of coordinates with a given precision. 
It was a function of x, z, the distance goal from the object as well as the precision in x-direction. 
The function is shown below. 
The precision variable was introduced because of the different requirement of precision based on what the Jetbot is following. 
If it is trying to reach a strawberry, the accuracy is very important, but not so much when the Jetbot is reaching the aruco marker.

.. literalinclude:: ../../src/MainController.py
    :pyobject: Robot.followItem