============
Introduction
============

This project is part of the MAS507 - Product Development and Project Management course at UiA and it contains the development of a strawberry picking robot. 
A strawberry picking robot is relevant in times where autonomous robots is becoming more and more common. 
This project will cover the development of such a robot. In the end, the robot was tested in a controlled and predefined artificial strawberry field.
A great part of the project includes product development and project management needed to develop a prototype in an efficient and cooperative way within a group.


Project description
-------------------

.. figure:: ./figures/strawberry_field.png
   :name: strawberriesfield
   :align: right
   :width: 8cm

   *Strawberry field*


The base for the robot was an NVIDIA Jetson Nano with a Jetbot 3D printed frame with a camera and a motor driver for up to 16 servo motors.
Much of the software was also provided by the supervisor, including aruco marker detection, strawberry detection, 
the ros environment and a main file where the students could implement the code needed to make the robot perform the task at hand.

* A robot arm, a gripper and a basket should be designed and 3D printed.
* Up to 3 servo motors can be used on the arm.
* There will be 8 red strawberries and 2 green ones. Only the red should be picked.
* The robot should pick the berries in :numref:`strawberriesfield` within 3 minutes from touching the first berry until the last berry is picked.
* The Jetbot should be programmed with python in a ROS environment running on an Ubuntu OS.
* Use forward and inverse kinematics to control the robot arm.
* Focus on reliability, not speed.
* Focus on product development and project management.