Results
=======

The video below shows the results from the test in the predefined strawberry field. It shows how the state machine explained in the software chapter works in practice.
Thanks to the computer vision algorithm, the video shows how the robot can pick the ripe berries with precision.

.. raw:: html

    <iframe width="704" height="396" src="https://www.youtube.com/embed/k57Qpbn7tn4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Mechanical design
*****************

In the end the robot arm assembly consisted of 2 joints and 3 servos. With the implemention of two main servos the quivering was really limited as shown. With modular design the parts were easy to change and redesign without the need in changing the 
belonging parts. 

It was desired to keep the system light resulting in the design of a quite small gripper. This made it difficult both to catch the strawberries as well as avoiding leafs and branches in the teeth. With longer teeth and 
a wider surface it deleted all pre-excisting problems while actually keeping the weight unchanged due to a more slim design. 
