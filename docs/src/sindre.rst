Mechanical Design
=================

For the arm and gripper some key properties were first determined to make the design of concept stage less comprehensive. A list of qualities are listed below.
 - Keep joints and servos to a minimum
 - Modular design with interchangeable components
 - Least amount of obstruction for camera
 - Easy to print
 - Rigid but light

The starting point of the project and the base for the mechanical design were a version of the Jetbot. The Jetbot is a differential driven mobile vehicle where the chassis
is 3D-printed. The mechanical task of the project is to implement a working assembly which can move strawberries from their plant and onto a basket at the Jetbot. The idea is 
to design and 3D-print all corresponding parts and brackets to develop the fully working strawberry picker.

.. contents::

Concept development
###################

First the group members scribbled down their concept ideas, and presented them to the rest of the group. The best ideas were then discussed until one idea were determined to be
designed in SolidWorks and then 3D-printed. 

In :numref:`konsept_even2` - :numref:`konsept_sindre3` some concept ideas are shown. These represent the complete system with arm, gripper and basket. In :numref:`radardiagram`
a radar chart of every concept can be seen. 


.. figure:: ./figures/konsept_even2.png 
   :name: konsept_even2
   :width: 500px
   :align: center

   *Concept 1 of complete system*

:numref:`konsept_even2` with front placement of basket and suspended arm with two joints. The arm catches the strawberries from top and push them down to the basket with a dynamic gripper. A 
counterweight is placed at the rear to prevent the robot from tipping forward. 


.. figure:: ./figures/konsept_sindre1.png 
   :name: konsept_sindre1
   :width: 500px
   :align: center

   *Concept 2 of complete system*

:numref:`konsept_sindre1` with front mounted arm with only one main joint. Catches the strawberry from underneath with a spork-designed gripper and delivers it to a basket 
at the rear of the robot. The gripper has a countersunk part which holds the strawberry in place when moving it to the basket. 



.. figure:: ./figures/konsept_sindre3.png 
   :name: konsept_sindre3
   :width: 500px
   :align: center

   *Concept 3 of complete system*

:numref:`konsept_sindre3` with top mounted arm which rotates like the tower on a military tank. A joint at the arm moving the end up and down with a gripper at the end. The gripper has a door at the bottom opening 
when the strawberry can be released to the side mounted basket. 

.. figure:: ./figures/radardiagram.png 
   :name: radardiagram
   :width: 700px
   :align: center

   *Radar chart of concepts*

In the radar chart the score goes from 0 to 5, where 5 is top score. The different test parameters and their description are listed below.
 - **Ease of design**, design process and CAD modeling
 - **Ease of production**, production with 3D printer and least amount of support
 - **Complexity**, influenced by the number of joints, servos and parts
 - **Controllability**, makes the software part less comprehensive
 - **Modularity**, interchangeable parts
 - **Rigidity**, influenced by stiffness, slack and shaking
 - **Weight**, reduces stress on components

Concept decision
****************

The concept shown in :numref:`konsept_sindre1`, with some slight modifications and parts used in other concept ideas were decided to further develop. It scored a total of 
28 out of 35 in the radar chart evaluation. An exploded view of the final robot arm concept can be seen in :numref:`render_first`

.. figure:: ./figures/render_exploded_view_numbered.png 
   :name: render_first
   :width: 700px
   :align: center

   *Exploded view of final decision of concept with Jetbot*

The designed parts are listed below and described in the following subsections. The numbers indicates the corresponding part in :numref:`render_first` 
 1. Gripper
 2. Gripper bracket
 3. Arm 
 4. Main bracket to Jetbot
 5. Axles

In addition to the robot arm the basket that store the berries is placed at the rear of the robot. It consist of the basket, holders and slanted frames to receive berries not released at center. 
The finished product can be seen in :numref:`final_design_gif` and consist of both the robot arm assembly and basket assembly connected to the Jetbot. 

.. figure:: ./figures/final_design.gif 
   :name: final_design_gif
   :width: 700px
   :align: center

   *Final design of robot arm assembly and basket assembly connected to Jetbot*

All parts are shown below in illustrative gifs, the parts can be seen in responsive 3D or the CAD-models can be downloaded by (ctrl + click) each gif, this way the link opens in a new tab.


Gripper
*******

.. figure:: ./figures/griper.png 
   :name: gripper_first
   :width: 300px
   :align: right

   *Early concept of gripper*

The gripper represents the part that embraces the strawberries and disassemble them from the bush and carries them to the basket. 
It should carry out caution with the strawberries, thereby not penetrate their skin or squish them keeping the robot clean and strawberries intact. 
It should not intervene with the bush itself possibly damage both the bush and the robot assembly. 

Many concepts were developed and evaluated for the gripper. From an early stage a concept similar to a manual blueberry picker and rake were developed, the first concept can be seen in :numref:`gripper_first` 

.. figure:: ./figures/robot_fall.gif 
   :name: robot_fall
   :align: right
   :width: 300px

   *Falling robot trying to lift the bush*

After printing it and testing it with the artificial strawberry bush, it was clear that the teeth were placed too close and with minimal spacing. 
This resulted in the gripper picking up every other leaf and thereby the bush itself, tipping the robot over which can be seen in :numref:`robot_fall` 

Another aspect was the demand of high precision due to the narrow gripper area. By widening the gripper the strawberry picking success rate increased immediately . 

.. figure:: ./figures/griper_stor_rev2.gif
   :name: griper_new
   :width: 500px
   :target: https://grabcad.com/library/gripper-200
   :align: center

   *Final design of gripper. ctrl + click to open in a 3D viewer*

In the final design shown in :numref:`griper_new` the gripper area is significantly wider. This reduced the need of high precision reducing the picking and moving time for each strawberry. The teeth now also goes significantly lower decreasing the risk 
of catching a leaf or branch. 


Gripper bracket
***************

The final concept includes two joints as opposed from one in concept 2 from :numref:`konsept_sindre1` As some strawberries hang low to the ground, a secondary outer joint was 
necessary to get the teeth of the gripper under the strawberries. With an outer secondary joint it was made possible to lock the berries in place while moving them to the basket.
As can be seen in :numref:`griper_bracket` it has a wall which prevents the berries from falling out from the gripper by locking them into place.

.. figure:: ./figures/bracket_arm.gif 
   :name: griper_bracket
   :width: 500px
   :target: https://grabcad.com/library/bracket-339
   :align: center

   *Final design of gripper bracket. ctrl + click to open in a 3D viewer*

On the rear of the bracket there is a connection key to the main arm. This locks the bracket to the arm preventing any kind of movement and rotation and thereby reducing slack. 
It also enables the gripper and bracket to be rotated if another concept or solution was to be tested or used. 
The arm and gripper bracket is connected with the connection key and nut and bolt. The protruding elements at the right are the connections for the servo connected to the axle rotating the axle. 

Arm
****

The arm is the part that translates the movement from the Jetbot to the gripper. The rotational movement comes from the axle at the main bracket. Due to the desire of modular design the gripper and arm was designed as separate parts. 
This also was necessary due to the demand of an outer joint. The final design of the arm can be seen in :numref:`arm` One side of the arm was designed flat reducing the need of filament in support. By designing it with one flat side it also
increased the downward angle making it easier for the gripper to slide below low-hanging strawberries. 

.. figure:: ./figures/arm_ny.gif
   :name: arm
   :width: 500px
   :target: https://grabcad.com/library/arm-152
   :align: center

   *Final design of arm. ctrl + click to open in a 3D viewer*


Main bracket
************

The main bracket is placed and connected to the Jetbot and offers the base for the robot arm assembly, it can be seen in :numref:`main_bracket` 

.. figure:: ./figures/hovedbrakett.gif
   :name: main_bracket
   :width: 500px
   :target: https://grabcad.com/library/main-bracket-1
   :align: center

   *Final design of main bracket. ctrl + click to open in a 3D viewer*



.. figure:: ./figures/main_bracket.png
   :name: main_bracket_fasten
   :width: 300px
   :align: right

   *Main bracket fastened to the Jetbot*

The main bracket is held in place by two screws and the sloping edge at the front of the Jetbot as can be seen in :numref:`main_bracket_fasten` To minimize the obstruction for the camera
aswell as reduce the weight and print time the middle section was cut out. The main bracket was also used as a handle while carrying the robot around.

At first it was designed and 3D printed with only one servo mounting. While one servo seemed sufficient at first, it started struggling gradually due to excessive wear resulting in shaking in the robot arm.
The shaking also induced looser fitment between the main bracket and the axle. The lack of fitment resulted in a lot of slack and even more wear at both the servo and fitment. Each servo was attached with two screws in the protruding elements. 
A comparison with one and two servos can be seen in :numref:`servos_sidebyside`

.. figure:: ./figures/servos_sidebyside.gif
   :name: servos_sidebyside
   :height: 9.45cm
   :align: center

   *Comparison between one and two main servos*

By designing one side flat there was little need of support in the 3D printing process. This reduced the printing time and filament used. 


Axle
****

The axles translates the torque from the servos to the arm and gripper. They utilize the standard servo spline with 25 teeth from the servos and acts as a D-shaft for the arm and gripper. 
This solution with spline and D-shaft worked really well and had little to no measurable slack. The axles, both long used for the arm and the short used for the gripper can be seen in :numref:`axles`

.. figure:: ./figures/akslinger.gif
   :name: axles
   :width: 500px
   :target: https://grabcad.com/library/axle-long-1
   :align: center

   *Final design of axles. ctrl + click to open in a 3D viewer*

.. figure:: ./figures/axle_d_spline.jpg
   :name: axle_d
   :width: 300px
   :align: right

   *Close-up of spline and D-shaft*

To get the perfect result with no slack between the axles, servos and arm/gripper it required some trials and tuning with the 3D printer and model. The end result was a difference of 0.2mm between the inner and outer part, i.e. shaft and arm.

The spline required high precision to fit to the servos, this meant that the axles had to be printed standing up. By printing in this manner it meant loosing some strength as the 3D printer prints layer by layer upwards.
3D printed parts are dependent of the adhesion between the layers to be tough, it turned out to be sufficiently strong and no axles were harmed during testing. 


Basket assembly
***************

The basket assembly holds every strawberry and has to carry at least 8 strawberries. The basket is placed at the rear of the robot and also act as a counterbalance when the robot arm assembly reach out to get the strawberries.
In :numref:`basket` the basket assembly can be seen with the basket, holders and slanted frames. 

.. figure:: ./figures/cartridge_assembly.gif
   :name: basket
   :width: 500px
   :target: https://grabcad.com/library/basket-assembly-1
   :align: center

   *Final design of basket assembly. ctrl + click to open in a 3D viewer*

At first the basket was printed as one body with integrated holders to be fastened to the chassis of the Jetbot. As the basket was removed from the 3D printer the holders broke off, instead of printing the whole basket one more time the holders
were redesigned and printed individually. With nuts and bolts the new holders were fastened to the chassis and basket.

As can be seen in :numref:`basket` the floor of the basket is patterned. The patterned floor reduces the rebound happening when the berries are released onto it, avoiding the berries falling out. 
An issue recorded with the widened gripper was the berries being released at the endpoint and hitting the edge of the basket. By implementing some slanted frames the berries could no longer miss the basket. 


Geometry
########

The robot has to know the geometry of the robot arm assembly to know how to approach the strawberries. This applies for the position of the berries in the vertical y-axis as the horizontal position is fixed.
Inverse kinematics has to be used to find the desired angles of the robot arm and the gripper.


.. image:: ./figures/geometry.png
   :width: 17cm


The y-position of the gripper can be calculated with the following equation:

.. math::
    y_{gripper} = L_{0y} + L_1\cdot sin(\theta_0) + L_2\cdot sin(\theta_1)\\

By using the y-position of the berry, the robot arm angle can be calculated:

.. math::
    \theta_0 = sin^{-1}\left(\frac{1}{L_1}\cdot(y_{berry} - L_2\cdot sin(\theta_1) - L_{0y})\right)\\

It is assumed that the gripper will be in a vertical position :math:`(\theta_1 = -90^o)` every time a strawberry is picked. The equation can then be simplified:

.. math::
    \theta_0 = sin^{-1}\left(\frac{1}{L_1}\cdot(y_1 + L_2 - L_{0y})\right)\\