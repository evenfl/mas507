#!/usr/bin/env python
"""
Main Control Node
"""
import rospy
import numpy as np
import os
from mas507.msg import ServoSetpoints
from cv2 import aruco
from std_msgs.msg import Int64, Float32
from sensor_msgs.msg import Image
from StrawberryDetector import StrawberryDetector
from enum import Enum

autoinc = 0
def auto():
    global autoinc
    autoval = autoinc
    autoinc += 1
    return autoval

class MainState(Enum):
    INIT = auto()
    PARK = auto()
    PICK_BERRY = auto()
    MOVE_TO_NEXT_BERRY = auto()
    CHANGE_SIDE = auto()
    FINISH = auto()

class PickBerryState(Enum):
    INIT = auto()
    FIND_BERRY = auto()
    APPROACH_BERRY = auto()
    POSITION_ROBOT_ARM = auto()
    MOVE_TO_PICKING_POSITION = auto()
    PICK = auto()
    BACK_UP = auto()
    POSITION_BERRY = auto()
    RELEASE_BERRY = auto()
    EXIT = auto()

class ChangeSideState(Enum):
    INIT = auto()
    TURN_RIGHT_1 = auto()
    MOVE_FORWARD = auto()
    TURN_RIGHT_2 = auto()
    EXIT = auto()

class Robot:

    def __init__(self, L_0z, L_0y, L_1, L_2, servoAngle0_Offset, servoAngle1_Offset):
        self.L_0z = L_0z
        self.L_0y = L_0y
        self.L_1 = L_1
        self.L_2 = L_2
        self.servoAngle0_Offset = servoAngle0_Offset
        self.servoAngle1_Offset = servoAngle1_Offset
        self.leftWheelSpeed = 306
        self.rightWheelSpeed = 306
        self.servoAngle0 = 45
        self.servoAngle1 = 45

    def setAngles(self, theta0, theta1):
        # Transforms global robot arm angles theta0 and theta1 to
        # local coordinate angles for the servo motors
        self.servoAngle0 = self.servoAngle0_Offset - theta0
        self.servoAngle1 = theta0 + self.servoAngle1_Offset - theta1
        if self.servoAngle1 < 5:
            # This is to avoid the servo motor to 
            # push towards the mechanical endpoint
            self.servoAngle1 = 5

    def setServoMotors(self):
        # Sets and publishes the speed of the wheels and the 
        # angles of the servo motors for the robot arm
        servoSetpoints = ServoSetpoints()
        servoSetpoints.leftWheel = self.leftWheelSpeed
        servoSetpoints.rightWheel = self.rightWheelSpeed
        servoSetpoints.servo1 = (self.servoAngle0+0.5)*(540-80)/180 + 80 #min=80 max=540
        servoSetpoints.servo2 = (180-self.servoAngle0)*(540-80)/180 + 80 #min=80 max=540
        servoSetpoints.servo3 = round(self.servoAngle1*(475-115)/180) + 115 #min=80 max=540
        pub_servoSetpoints.publish(servoSetpoints)

    def stop(self):
        # Sets the speed to 0 m/s
        self.leftWheelSpeed = 306
        self.rightWheelSpeed = 306

    def turnRight(self):
        # Turns right around it's own axis
        self.leftWheelSpeed = 327
        self.rightWheelSpeed = self.leftWheelSpeed

    def turnLeft(self):
        # Turns left around it's own axis
        self.leftWheelSpeed = 296
        self.rightWheelSpeed = self.leftWheelSpeed

    def followItem(self, x, z, stopdist, prcsn):
        # The robot moves towards the x and z coordinates until
        # a given stop distance and precision is reached
        if z < 0.4:
            precision = prcsn
        else:
            precision = 0.1 
        if x > precision:
            # Turns slowly to the right
            self.leftWheelSpeed = 320
            self.rightWheelSpeed = 320
        elif x < -precision:
            # Turns slowly to the left
            self.leftWheelSpeed = 302
            self.rightWheelSpeed = 302
        else:
            if z < stopdist:
                # Sets speed to 0 m/s
                self.leftWheelSpeed = 306
                self.rightWheelSpeed = self.leftWheelSpeed
            else:
                self.moveForward()

    def backUp(self):
        self.leftWheelSpeed = 293
        self.rightWheelSpeed = 330

    def moveForward(self):
        self.leftWheelSpeed = 328
        self.rightWheelSpeed = 296

    def moveRobotArm(self,theta0_0, theta0d_0, theta0_1, theta0d_1, theta1_0, theta1d_0, theta1_1, theta1d_1, T, t):
        A = [[1, 0, 0, 0],
            [0, 1, 0, 0],
            [1, T, T**2, T**3],
            [0, 1, 2*T, 3*T**2]]
        x_0 = [theta0_0, theta0d_0, theta0_1, theta0d_1]
        x_1 = [theta1_0, theta1d_0, theta1_1, theta1d_1]
        b_0 = np.dot(np.linalg.inv(A),x_0)
        b_1 = np.dot(np.linalg.inv(A),x_1)
        theta0 = b_0[0] + b_0[1]*t + b_0[2]*t**2 + b_0[3]*t**3
        theta1 = b_1[0] + b_1[1]*t + b_1[2]*t**2 + b_1[3]*t**3
        self.setAngles(theta0, theta1)

def moveForwardTime(distance):
    t_start = 0.2
    t_moveForward = t_start + distance/0.092
    return t_moveForward
        
# Create callback to handle data from subscriber
def callbackx(msg):
    global arucoX
    if msg.data:
        arucoX = msg.data
def callbacky(msg):
    global arucoY
    if msg.data:
        arucoY = msg.data
def callbackz(msg):
    global arucoZ
    if msg.data:
        arucoZ = msg.data
def callbackid(msg):
    global arucoID
    if msg.data:
        arucoID = msg.data

if __name__ == '__main__':
    try:
        # Init ROS node
        rospy.init_node('mainController', anonymous=True)

        # Publishers
        pub_servoSetpoints = rospy.Publisher('servoSetpoints', ServoSetpoints, queue_size=1)
        
        #Initialize coordinates to the aruco marker
        arucoX = 0.0
        arucoY = 0.0
        arucoZ = 1.0
        arucoID = 0

        # Publishers
        pub_strawberry_detection = rospy.Publisher('strawberry_detection', Image, queue_size=1)
    
        # Strawberry detector
        intrinsicCalibration =  np.load('%s/catkin_ws/src/mas507/data/intrinsicCalibration.npz' % (os.path.expanduser("~")))
        strawberryDetector = StrawberryDetector(pub_strawberry_detection, intrinsicCalibration['mtx'], intrinsicCalibration['dist'])

        # Subscribers
        sub_calibrated = rospy.Subscriber('image_calibrated', Image, strawberryDetector.callback)
        arucoXSub=rospy.Subscriber('x', Float32, callbackx)
        arucoYSub=rospy.Subscriber('y', Float32, callbacky)
        arucoZSub=rospy.Subscriber('z', Float32, callbackz)
        arucoIDSub=rospy.Subscriber('id', Int64, callbackid)

        # Start Synchronous ROS node execution
        t = 0
        t_0 = t
        t_ID = 0
        dt = 0.05 # 20 Hz
        rate = rospy.Rate(int(1/dt))
        t_moveToNextBerry = moveForwardTime(0.15)
        t_changeSide = moveForwardTime(0.6)
        t_turnLeft = 1.16 # Time to turn 90 degrees to the left
        t_turnRight = 1.02 # Time to turn 90 degrees to the right
        t_moveToPickingPosition = 0.53 # Time to drive forward after positioning the robot arm
        t_backUp = 0.8 # Time of backing up after picking a strawberry
        T_arm = 3 # Time to move the robot arm from one angle to another
        T_gripper = 1 # Time to move the gripper from one angle to another

        arucoPrcsn = 0.1 # 10 cm precision in x direction when approaching an arucomarker
        L_0z = -0.01 # Horizontal length from the camera to servo 1
        L_0y = 0.0551 # Vertical length from the camera to servo 1
        L_1 = 0.14 # Length from servo 1 to servo 2
        L_2 = 0.02 # Length from servo 2 to the focal point of the gripper
        servoAngle0_Offset = 136 # Mechanical angular offset for the servomotors on joint 1
        servoAngle1_Offset = 70 # Offset for joint 2
        counter0 = 0
        counter1 = 0

        #Initialize myRobot which is a Robot class
        myRobot = Robot(L_0z, L_0y, L_1, L_2, servoAngle0_Offset, servoAngle1_Offset)

        # The min and max distance where the robot can start the picking process
        minPickingDistance = (myRobot.L_0z + myRobot.L_1 + myRobot.L_2) + 0.02
        maxPickingDistance = 0.25 - 0.005

        # Initialize the states
        mainState = MainState.INIT
        pickBerryState = PickBerryState.INIT
        changeSideState = ChangeSideState.INIT
        
        theta1 = 45
        theta0 = 45
        theta0_0 = 45
        theta1_0 = 45
        
        while not rospy.is_shutdown():
            
            if mainState == MainState.INIT:
                mainState = MainState.PARK
                t_0 = t
                counter0 = 0
                counter1 = 0
                
            if mainState == MainState.PARK:
                # Stops the robot and sets the robot arm to a 45 degree angle
                myRobot.moveRobotArm(45, 0, 45, 0, 45, 0, 45, 0, 0.5, (t - t_0))
                myRobot.stop()
                if t-t_0 > 0.5:
                    if (arucoID == 1 or arucoID == 3):
                        if arucoX > 0.1:
                            myRobot.leftWheelSpeed = 322
                            myRobot.rightWheelSpeed = 318
                        elif arucoX < -0.1:
                            myRobot.leftWheelSpeed = 301
                            myRobot.rightWheelSpeed = 303
                        else:
                            arucoZ_0 = arucoZ
                            mainState = MainState.MOVE_TO_NEXT_BERRY
                            t_0 = t
                            print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                    
                    elif arucoID == 2:
                        mainState = MainState.CHANGE_SIDE
                        changeSideState = ChangeSideState.MOVE_FORWARD

                    if arucoZ < 0.6 and arucoID == 1 and arucoZ != 0.0 and abs(arucoX) < arucoPrcsn:
                        mainState = MainState.CHANGE_SIDE
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                    
                    if arucoID != 1 and arucoID != 3:
                        counter1 = counter1 + 1
                        if counter1 > 5 and counter1 <= 10:
                            myRobot.turnRight()
                        elif counter1 > 10:
                            counter1 = 0
                    else:
                        counter1 = 0

                    if np.sqrt(arucoZ*arucoZ+arucoX*arucoX) < 0.45 and arucoID == 3:
                        counter0 = counter0 + 1
                        if counter0 >= 10:
                            mainState = MainState.FINISH
                            t_0 = t
                            print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                            counter0 = 0
                    else:
                        counter0 = 0

            # Moves to the next berry by following the aruco marker
            if mainState == MainState.MOVE_TO_NEXT_BERRY:
                if t-t_0 > 0.5:
                    myRobot.followItem(arucoX, arucoZ, arucoZ_0 - 0.17, arucoPrcsn)
                    
                    if arucoZ_0 - arucoZ > 0.17:
                        myRobot.stop()
                        arucoZ_0 = arucoZ
                       
                        mainState = MainState.PICK_BERRY
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

            # Pick berry state starts
            if mainState == MainState.PICK_BERRY:
                if pickBerryState == PickBerryState.INIT:
                    pickBerryState = PickBerryState.FIND_BERRY
                    t_0 = t
                    print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                    
                if pickBerryState == PickBerryState.FIND_BERRY:    
                    if t - t_0 <= t_turnRight:
                        myRobot.turnRight()                    
                    
                    else:
                        myRobot.stop()

                        if strawberryDetector.z == 0.0:
                            counter0 = counter0 + 1
                            if counter0 >= 5:
                                pickBerryState = PickBerryState.EXIT
                                t_0 = t
                                print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                                counter0 = 0
                                theta0 = 45
                                theta1 = 45
                        
                        else:
                            pickBerryState = PickBerryState.APPROACH_BERRY
                            t_0 = t
                            print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                            counter0 = 0
                
                if pickBerryState == PickBerryState.APPROACH_BERRY:
                    berryDist = 1
                    prcsn = 0.02
                    
                    if t-t_0 > 1 and strawberryDetector.z != 0.0:
                        myRobot.followItem( strawberryDetector.x, strawberryDetector.z, maxPickingDistance, prcsn )
                        counter0 = 0
                    
                    elif t-t_0 > 1:
                        myRobot.stop()
                        counter0 = counter0 + 1
                        if counter0 >= 5:
                            counter0 = 0
                            pickBerryState = PickBerryState.EXIT
                            theta0 = 45
                            theta1 = 45
                            t_0 = t
                            print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                    if t-t_0 > 1:
                        if strawberryDetector.z <= maxPickingDistance and strawberryDetector.z > (myRobot.L_0z + myRobot.L_1 + myRobot.L_2) and abs(strawberryDetector.x) < prcsn and strawberryDetector.z != 0.0:
                            # if the strawberry is within picking position
                            myRobot.stop()
                            t_0 = t
                            theta0_0 = 45
                            theta1_0 = 45
                            
                            theta0 = 180/np.pi * np.arcsin( 1/myRobot.L_1 * ( strawberryDetector.y + myRobot.L_2 - myRobot.L_0y ) )
                            
                            # Saturation for theta0:
                            if theta0 != theta0: # if theta0 == nan
                                theta0 = 0
                            if theta0 > myRobot.servoAngle0_Offset:
                                theta0 = myRobot.servoAngle0_Offset
                            elif theta0 < myRobot.servoAngle0_Offset-180:
                                theta0 = myRobot.servoAngle0_Offset-180
                            
                            theta1 = theta0-(180-myRobot.servoAngle1_Offset)
                            
                            # Saturation for theta1:
                            if theta1 < -(180-myRobot.servoAngle1_Offset):
                                theta1 = -(180-myRobot.servoAngle1_Offset)
                            
                            str_z = strawberryDetector.z
                            counter0 = 0

                            pickBerryState = PickBerryState.POSITION_ROBOT_ARM
                            t_0 = t
                            print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                        elif strawberryDetector.z < minPickingDistance and strawberryDetector.z != 0.0:
                            myRobot.backUp()

                if pickBerryState == PickBerryState.POSITION_ROBOT_ARM:
                    myRobot.moveRobotArm(theta0_0, 0, theta0, 0, theta1_0, 0, theta1, 0, T_arm, (t - t_0))
                    
                    if t-t_0 > T_arm:
                        theta0_0 = theta0
                        theta1_0 = theta1
                        pickBerryState = PickBerryState.MOVE_TO_PICKING_POSITION
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                if pickBerryState == PickBerryState.MOVE_TO_PICKING_POSITION:
                    myRobot.moveForward()

                    if t - t_0 > t_moveToPickingPosition:
                        myRobot.stop()
                        
                        theta0_0 = theta0
                        theta1_0 = theta1
                        theta1 = theta0 + 60
                    
                        pickBerryState = PickBerryState.PICK
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                if pickBerryState == PickBerryState.PICK:
                    myRobot.moveRobotArm(theta0_0, 0, theta0, 0, theta1_0, 0, theta1, 0, T_gripper, (t - t_0))
                    
                    if t-t_0 > T_gripper:
                        pickBerryState = PickBerryState.BACK_UP
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                if pickBerryState == PickBerryState.BACK_UP:
                    myRobot.backUp()

                    if t - t_0 > t_backUp:
                        myRobot.stop()
                    
                        theta0_0 = theta0
                        theta1_0 = theta1
                        theta0 = myRobot.servoAngle0_Offset
                        theta1 = theta1_0 + (theta0-theta0_0)
                    
                        pickBerryState = PickBerryState.POSITION_BERRY
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
  
                if pickBerryState == PickBerryState.POSITION_BERRY:
                    myRobot.moveRobotArm(theta0_0, 0, theta0, 0, theta1_0, 0, theta1, 0, T_arm, (t - t_0))                
                    
                    if t-t_0 > T_arm:
                        theta0_0 = theta0
                        theta1_0 = theta1
                        theta1 = theta0
                    
                        pickBerryState = PickBerryState.RELEASE_BERRY
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                if pickBerryState == PickBerryState.RELEASE_BERRY:
                    myRobot.moveRobotArm(theta0_0, 0, theta0, 0, theta1_0, 0, theta1, 0, T_gripper, (t - t_0))                
                    
                    if t-t_0 > T_gripper:
                        pickBerryState = PickBerryState.EXIT
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                if pickBerryState == PickBerryState.EXIT:
                    myRobot.turnLeft()
                    myRobot.moveRobotArm(theta0, 0, 45, 0, theta0, 0, 45, 0, t_turnLeft, (t - t_0))
                
                    if t - t_0 > t_turnLeft:
                        myRobot.stop()
                    
                        pickBerryState = PickBerryState.INIT
                        mainState = MainState.INIT
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

            if mainState == MainState.CHANGE_SIDE: 
                if changeSideState == ChangeSideState.INIT:                
                    if t - t_0 < moveForwardTime(0.2):
                        myRobot.moveForward()
                    else:
                        myRobot.stop()
                        counter1 = 0
                        changeSideState = ChangeSideState.TURN_RIGHT_1
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                
                if changeSideState == ChangeSideState.TURN_RIGHT_1:
                    myRobot.stop()
                    
                    if arucoID != 2:
                        counter1 = counter1 + 1
                        if counter1 > 5 and counter1 <= 10:
                            myRobot.turnRight()
                        elif counter1 > 10:
                            counter1 = 0
                    else:
                        counter1 = 0


                    if arucoID == 2:# t - t_0 > t_turnRight:
                        counter0 = 0
                        changeSideState = ChangeSideState.MOVE_FORWARD
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                
                if changeSideState == ChangeSideState.MOVE_FORWARD:
                    myRobot.followItem(arucoX, arucoZ, 0.17, arucoPrcsn)
                    
                    if arucoZ < 0.18 and arucoZ != 0.0:
                        counter0 = counter0 + 1
                        if counter0 >= 3:
                            changeSideState = ChangeSideState.TURN_RIGHT_2
                            t_0 = t
                            print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                    else:
                        counter0 = 0

                if changeSideState == ChangeSideState.TURN_RIGHT_2:
                    myRobot.turnRight()
                    
                    if t - t_0 > t_turnRight+0.1:
                        changeSideState = ChangeSideState.EXIT
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                
                if changeSideState == ChangeSideState.EXIT:
                    myRobot.moveForward()
                    if t - t_0 > moveForwardTime(0.09):
                        changeSideState = ChangeSideState.INIT
                        mainState = MainState.INIT
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

            if mainState == MainState.FINISH:
                myRobot.moveForward()
                if t - t_0 > moveForwardTime(0.07):
                    myRobot.stop()

            myRobot.setServoMotors() # Sets speeds and angles to the motors

            if t-t_ID >= 0.5:
                # Reset aruco ID
                t_ID = t
                arucoID = 0

            # Sleep remaining time
            rate.sleep()
            t = t + dt
        
    except rospy.ROSInterruptException:
        pass