#!/usr/bin/env python
"""
Testing class
"""
import numpy as np

class Robot:

    def __init__(self, L_0x, L_0y, L_1, L_2, servoAngle1_Offset, servoAngle2_Offset):
        self.L_0x = L_0x
        self.L_0y = L_0y
        self.L_1 = L_1
        self.L_2 = L_2
        self.servoAngle1_Offset = servoAngle1_Offset
        self.servoAngle2_Offset = servoAngle2_Offset
        self.leftWheelSpeed = 306
        self.rightWheelSpeed = 306
        self.servoAngle1 = 0
        self.servoAngle2 = 0

    def setSpeed(self, leftWheelSpeed, rightWheelSpeed):
        self.leftWheelSpeed = leftWheelSpeed
        self.rightWheelSpeed = rightWheelSpeed

    def setAngles(self, theta1, theta2):
        self.servoAngle1 = theta1 - servoAngle1_Offset
        self.servoAngle2 = - theta1 + theta2 - servoAngle2_Offset

    def setServoMotors(self):
        #servoSetpoints = ServoSetpoints()
        #servoSetpoints.leftWheel = self.leftWheelSpeed
        #servoSetpoints.rightWheel = self.rightWheelSpeed
        #servoSetpoints.servo1 = self.servoAngle1*(540-80)/180 + 80 #min=80 max=540
        #servoSetpoints.servo2 = self.servoAngle2*(540-80)/180 + 80 #min=80 max=540
        #pub_servoSetpoints.publish(servoSetpoints)
        print('\nspeed_l={}, speed_r={}, ang1={}, ang2={}'.format(self.leftWheelSpeed, self.rightWheelSpeed, self.servoAngle1, self.servoAngle2))

    def runServoMotors(self, leftWheelSpeed, rightWheelSpeed, t_run):
        self.leftWheelSpeed = leftWheelSpeed
        self.rightWheelSpeed = rightWheelSpeed
        self.setServoMotors()
        #rospy.sleep(t_run)
        global t
        t = t + t_run
        print('\nSleep for t_run={}'.format(t_run))
        self.leftWheelSpeed = 306
        self.rightWheelSpeed = self.leftWheelSpeed
        self.setServoMotors()
        print('hi')
    
    def turnLeft(self):
        t_run = 1.19
        self.leftWheelSpeed = 327
        self.rightWheelSpeed = self.leftWheelSpeed
        self.setServoMotors()
        #rospy.sleep(t_run)
        global t
        t = t + t_run
        print('\nSleep for t_run={} at time {}'.format(t_run, t))
        self.leftWheelSpeed = 306
        self.rightWheelSpeed = self.leftWheelSpeed
        self.setServoMotors()
        print('\nTurned left 90 degrees')

    def turnRight(self):
        t_run = 1.23
        self.leftWheelSpeed = 296
        self.rightWheelSpeed = self.leftWheelSpeed
        self.setServoMotors()
        #rospy.sleep(t_run)
        global t
        t = t + t_run
        print('\nSleep for t_run={} at time {}'.format(t_run, t))
        self.leftWheelSpeed = 306
        self.rightWheelSpeed = self.leftWheelSpeed
        self.setServoMotors()
        print('\nTurned right 90 degrees')

    def pickStrawberry(self, strawberry_x, strawberry_y):
        theta1 = 180/np.pi * np.arcsin( 1/self.L_1 * ( strawberry_y + 2*self.L_2 - self.L_0y ) )
        theta2 = -90
        self.setAngles(theta1,theta2)
        print('\nPicking up strawberry with theta1={} and theta2={}. Ang1={},  Ang2={}\n'.format(theta1, theta2, self.servoAngle1, self.servoAngle2))
        gripperX = self.L_0x + self.L_1*np.cos(theta1*np.pi/180) + self.L_2*np.cos(theta2*np.pi/180)
        print('gripperX = {}'.format(gripperX))
        dx = strawberry_x - gripperX
        t_run = 0.3*dx/0.05
        self.leftWheelSpeed = 330
        self.rightWheelSpeed = 293
        self.setServoMotors()
        #rospy.sleep(t_run)
        global t
        t = t + t_run
        print('\nSleep for t_run={} at time {}'.format(t_run, t))
        self.leftWheelSpeed = 306
        self.rightWheelSpeed = self.leftWheelSpeed
        self.setServoMotors()

    def followItem(self, x, y, z, stopdist, prcsn):
        dist = np.sqrt(x*x+z*z)
        print('x={}, y={}, z={}, dist={}'.format(x, y, z, dist))

        precision = 0.1

        if dist < 0.4:
            precision = prcsn
        
        if dist > stopdist:
            if x > precision:
                self.leftWheelSpeed = 322 + x*30
                self.rightWheelSpeed = 306
            elif x < -precision:
                self.leftWheelSpeed = 306
                self.rightWheelSpeed = 300 + x*30
            else:
                self.leftWheelSpeed = 320 + 50*z
                self.rightWheelSpeed = 306 - 45*z
        else:
            self.leftWheelSpeed = 306
            self.rightWheelSpeed = self.leftWheelSpeed
        self.setServoMotors()


L_0x = -0.02 # Horizontal length from the camera to servo 1
L_0y = 0.04 # Vertical length from the camera to servo 1
L_1 = 0.14 # Length from servo 1 to servo 2
L_2 = 0.04 # Length from servo 2 to the focal point of the gripper
servoAngle1_Offset = -20
servoAngle2_Offset = -120

t = 0

myRobot = Robot(L_0x, L_0y, L_1, L_2, servoAngle1_Offset, servoAngle2_Offset)

#myRobot.runServoMotors(0,0,0)
myRobot.turnLeft()
myRobot.turnRight()

strawberry_x = 0.15
strawberry_y = 0.05
myRobot.pickStrawberry(strawberry_x, strawberry_y)
myRobot.followItem(1,1,1,1,1)

