#!/usr/bin/env python
"""
Main Control Node
"""
import rospy
import numpy as np
import os
from mas507.msg import ServoSetpoints
from cv2 import aruco
from std_msgs.msg import Int64, Float32
from sensor_msgs.msg import Image
from StrawberryDetector import StrawberryDetector
from enum import Enum

autoinc = 0
def auto():
    global autoinc
    autoval = autoinc
    autoinc += 1
    return autoval

class MainState(Enum):
    INIT = auto()
    PARK = auto()
    PICK_BERRY = auto()
    MOVE_TO_NEXT_BERRY = auto()
    CHANGE_SIDE = auto()
    FINISH = auto()

class PickBerryState(Enum):
    INIT = auto()
    FIND_BERRY = auto()
    APPROACH_BERRY = auto()
    POSITION_ROBOT_ARM = auto()
    MOVE_TO_PICKING_POSITION = auto()
    PICK = auto()
    BACK_UP = auto()
    POSITION_BERRY = auto()
    RELEASE_BERRY = auto()
    EXIT = auto()

class ChangeSideState(Enum):
    INIT = auto()
    TURN_RIGHT = auto()
    MOVE_FORWARD = auto()
    EXIT = auto()

class Robot:

    def __init__(self, L_0z, L_0y, L_1, L_2, servoAngle0_Offset, servoAngle1_Offset):
        self.L_0z = L_0z
        self.L_0y = L_0y
        self.L_1 = L_1
        self.L_2 = L_2
        self.servoAngle0_Offset = servoAngle0_Offset
        self.servoAngle1_Offset = servoAngle1_Offset
        self.leftWheelSpeed = 306
        self.rightWheelSpeed = 306
        self.servoAngle0 = 0
        self.servoAngle1 = 0

    def setAngles(self, theta0, theta1):
        self.servoAngle0 = self.servoAngle0_Offset - theta0
        self.servoAngle1 = theta0 + self.servoAngle1_Offset - theta1
        if self.servoAngle1 < 5:
            self.servoAngle1 = 5

    def setServoMotors(self):
        servoSetpoints = ServoSetpoints()
        servoSetpoints.leftWheel = self.leftWheelSpeed
        servoSetpoints.rightWheel = self.rightWheelSpeed
        servoSetpoints.servo1 = self.servoAngle0*(540-80)/180 + 80 #min=80 max=540
        servoSetpoints.servo2 = (180-self.servoAngle0)*(540-80)/180 + 80 #min=80 max=540
        servoSetpoints.servo3 = round(self.servoAngle1*(475-115)/180) + 115 #min=80 max=540
        pub_servoSetpoints.publish(servoSetpoints)

    def stop(self):
        self.leftWheelSpeed = 306
        self.rightWheelSpeed = 306

    def turnRight(self):
        self.leftWheelSpeed = 327
        self.rightWheelSpeed = self.leftWheelSpeed

    def turnLeft(self):
        self.leftWheelSpeed = 296
        self.rightWheelSpeed = self.leftWheelSpeed

    def followItem(self, x, z, stopdist, prcsn):
        precision = 0.1
        if z < 0.4:
            precision = prcsn
        if x > precision:
            self.leftWheelSpeed = 320
            self.rightWheelSpeed = 320
        elif x < -precision:
            self.leftWheelSpeed = 302
            self.rightWheelSpeed = 302
        else:
            if z < stopdist:
                self.leftWheelSpeed = 306
                self.rightWheelSpeed = self.leftWheelSpeed
            else:
                self.moveForward()
                #self.leftWheelSpeed = 330 - stopdist*(330-306)/z
                #self.rightWheelSpeed = 296 - stopdist*(296-306)/z

    def backUp(self):
        self.leftWheelSpeed = 293
        self.rightWheelSpeed = 330

    def moveForward(self):
        self.leftWheelSpeed = 328
        self.rightWheelSpeed = 296

    def moveRobotArm(self,theta0_0, theta0d_0, theta0_1, theta0d_1, theta1_0, theta1d_0, theta1_1, theta1d_1, T, t):
        A = [[1, 0, 0, 0], 
            [0, 1, 0, 0],
            [1, T, T**2, T**3],
            [0, 1, 2*T, 3*T**2]]
        x_0 = [theta0_0, theta0d_0, theta0_1, theta0d_1]
        x_1 = [theta1_0, theta1d_0, theta1_1, theta1d_1]
        b_0 = np.dot(np.linalg.inv(A),x_0)
        b_1 = np.dot(np.linalg.inv(A),x_1)
        theta0 = b_0[0] + b_0[1]*t + b_0[2]*t**2 + b_0[3]*t**3
        theta1 = b_1[0] + b_1[1]*t + b_1[2]*t**2 + b_1[3]*t**3
        self.setAngles(theta0, theta1)

    def getCurrentServoAngles(self):
        theta0_0 = myRobot.servoAngle0_Offset - myRobot.servoAngle0
        theta1_0 = theta0_0 + myRobot.servoAngle1_Offset - myRobot.servoAngle1
        return theta0_0, theta1_0

def moveForwardTime(distance):
    t_start = 0.2
    t_moveForward = t_start + distance/0.092
    return t_moveForward
        
# Create callback to handle data from subscriber
def callbackx(msg):
    global arucoX
    global arucoTime
    global t
    if msg.data:
        arucoX = msg.data
        arucoTime = t
def callbacky(msg):
    global arucoY
    if msg.data:
        arucoY = msg.data
def callbackz(msg):
    global arucoZ
    if msg.data:
        arucoZ = msg.data
def callbackid(msg):
    global arucoID
    if msg.data:
        arucoID = msg.data



#Initialize coordinates to the aruco marker
arucoX = 0.0
arucoY = 0.0
arucoZ = 1.0
arucoID = 0


if __name__ == '__main__':
    try:
        # Init ROS node
        rospy.init_node('mainController', anonymous=True)

        # Publishers
        pub_servoSetpoints = rospy.Publisher('servoSetpoints', ServoSetpoints, queue_size=1)

        # Publishers
        pub_strawberry_detection = rospy.Publisher('strawberry_detection', Image, queue_size=1)
    
        # Strawberry detector
        intrinsicCalibration =  np.load('%s/catkin_ws/src/mas507/data/intrinsicCalibration.npz' % (os.path.expanduser("~")))
        strawberryDetector = StrawberryDetector(pub_strawberry_detection, intrinsicCalibration['mtx'], intrinsicCalibration['dist'])

        # Subscribers
        sub_calibrated = rospy.Subscriber('image_calibrated', Image, strawberryDetector.callback)
        arucoXSub=rospy.Subscriber('x', Float32, callbackx)
        arucoYSub=rospy.Subscriber('y', Float32, callbacky)
        arucoZSub=rospy.Subscriber('z', Float32, callbackz)
        arucoIDSub=rospy.Subscriber('id', Int64, callbackid)

        # Start Synchronous ROS node execution
        t = 0
        t_0 = t
        t_moveToNextBerry = moveForwardTime(0.15)
        t_changeSide = moveForwardTime(0.6)
        t_turnLeft = 1.19
        t_turnRight = 1.02#1.23
        t_backUp = 1
        T = 2
        T_arm = 3
        T_gripper = 2
        dt = 0.05
        rate = rospy.Rate(int(1/dt))

        arucoTime = 0.0
        arucoPrcsn = 0.2
        L_0z = -0.01 # Horizontal length from the camera to servo 1
        L_0y = 0.0551 # Vertical length from the camera to servo 1
        L_1 = 0.145 # Length from servo 1 to servo 2
        L_2 = 0.045 # Length from servo 2 to the focal point of the gripper
        servoAngle0_Offset = 136
        servoAngle1_Offset = 70
        counter = 0
        counter1 = 0
        ID0_counter = 0
        ID2_counter = 0

        #Initialize myRobot which is a Robot class
        myRobot = Robot(L_0z, L_0y, L_1, L_2, servoAngle0_Offset, servoAngle1_Offset)
        minPickingDistance = (myRobot.L_0z + myRobot.L_1 + myRobot.L_2)
        maxPickingDistance = 0.25
        
        TESTING = 69

        mainState = MainState.INIT
        pickBerryState = PickBerryState.INIT
        changeSideState = ChangeSideState.INIT
        #myRobot.setAngles(40,40)

        strawberryDetector.z = 0.0
        
        theta1 = 0
        theta0 = 0
        theta0_0 = 0#myRobot.servoAngle0_Offset - myRobot.servoAngle0
        theta1_0 = 0#130#theta0_0 + myRobot.servoAngle1_Offset - myRobot.servoAngle1
        
        
        while not rospy.is_shutdown():
            #print('{}'.format(arucoX))

            # This state is for testing
            if mainState == TESTING:
                #myRobot.moveRobotArm(theta0_0, 0, theta0, 0, theta1_0, 0, theta1, 0, T_gripper, (t - t_0))
                if t-t_0 > T_gripper:
                    rospy.sleep(2)
                    t_0 = t
                    theta0_0 = theta0
                    theta1_0 = theta1
                    if theta1 == 0:
                        theta0 = 90
                        theta1 = 45
                    else:
                        theta0 = 0
                        theta1 = 0
            
            #elif mainState == TESTING:
            #    myRobot.stop()
            
            if mainState == MainState.INIT:
                theta0_0, theta1_0 = myRobot.getCurrentServoAngles()
                mainState = MainState.PARK
                print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                t_0 = t
                counter = 0
                started = False
                started1 = False
                
            # Stops the robot and sets the robot arm to a vertical position
            if mainState == MainState.PARK:
                myRobot.moveRobotArm(45, 0, 45, 0, 45, 0, 45, 0, 1, (t - t_0))
                myRobot.stop()
                if t-t_0 > 1:
                    #print('\n{}, {}, {}, at time {}, aruco {}, {}'.format(mainState, pickBerryState, changeSideState,t, arucoX, arucoZ))
                    if arucoID == 1 or arucoID == 3:
                        if abs(arucoX) > 0.1:
                            #if started == True:
                            myRobot.followItem(arucoX, arucoZ, 0, arucoPrcsn)
                            if started == False and arucoX > 0.1 and 0 == 1:
                                myRobot.leftWheelSpeed = 326
                                myRobot.rightWheelSpeed = myRobot.leftWheelSpeed
                                started = True
                            elif started == False and arucoX < -0.1 and 0 == 1:
                                myRobot.leftWheelSpeed = 302
                                myRobot.rightWheelSpeed = myRobot.leftWheelSpeed
                                started = True
                            if arucoZ > 0.6 and arucoID == 1:
                                arucoZ_0 = arucoZ
                                started = False
                                started1 = False
                                mainState = MainState.MOVE_TO_NEXT_BERRY
                                t_0 = t
                                print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                        if arucoID == 1 and arucoZ != 0.0 and abs(arucoX) < arucoPrcsn and arucoZ < 0.6:
                            counter1 = counter1 + 1
                            if counter1 >= 3:
                                mainState = MainState.CHANGE_SIDE
                                t_0 = t
                                print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                    

                        if arucoZ < 0.4 and arucoID == 3 and arucoZ != 0.0 and abs(arucoZ) < 0.1:
                            counter = counter + 1
                            if counter >= 5:
                                mainState = MainState.FINISH
                                t_0 = t
                                print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                                counter = 0
                        else:
                            counter = 0
                        
                        ID0_counter = 0
                        started1 = False
                    
                    else:# arucoID != 1 and arucoID != 3:
                        ID0_counter = ID0_counter + 1
                        if ID0_counter >= 20:
                            if started1 == True:
                                myRobot.leftWheelSpeed = 320
                                myRobot.rightWheelSpeed = myRobot.leftWheelSpeed
                            elif started1 == False:
                                myRobot.leftWheelSpeed = 326
                                myRobot.rightWheelSpeed = myRobot.leftWheelSpeed
                                started1 = True
                    #else:
                    #    ID0_counter = 0
                    #    started1 = False

                    if arucoID == 2:
                        ID2_counter = ID2_counter + 1
                        if ID2_counter >= 5:
                            mainState = MainState.CHANGE_SIDE
                            changeSideState = ChangeSideState.MOVE_FORWARD
                    else:
                        ID2_counter = 0

            # Moves to the next berry by following the aruco marker
            if mainState == MainState.MOVE_TO_NEXT_BERRY:
                if t-t_0 > 0.5:
                    if arucoID == 1 or arucoID == 3:
                        myRobot.followItem(arucoX, arucoZ, arucoZ_0 - 0.16, arucoPrcsn)
                        ID0_counter = 0
                        #if arucoZ_0 - arucoZ > 0.16:
                        #    myRobot.stop()
                        #    arucoZ_0 = arucoZ
                        
                        #    mainState = MainState.PICK_BERRY
                        #    t_0 = t
                        #   print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                    else:
                        ID0_counter = ID0_counter + 1
                        if ID0_counter >= 20:
                            myRobot.leftWheelSpeed = 322
                            myRobot.rightWheelSpeed = myRobot.leftWheelSpeed

                    if arucoZ_0 - arucoZ > 0.16 and (arucoID == 1 or arucoID == 3):
                        myRobot.stop()
                        arucoZ_0 = arucoZ
                       
                        mainState = MainState.PICK_BERRY
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

            # Pick berry state starts
            if mainState == MainState.PICK_BERRY:
                if pickBerryState == PickBerryState.INIT:
                    pickBerryState = PickBerryState.FIND_BERRY
                    t_0 = t
                    print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                    
                if pickBerryState == PickBerryState.FIND_BERRY:    
                    if t - t_0 <= t_turnRight:
                        myRobot.turnRight()                    
                    
                    else:
                        myRobot.stop()

                        if strawberryDetector.z == 0.0:
                            counter = counter + 1
                            if counter >= 5:
                                pickBerryState = PickBerryState.EXIT
                                t_0 = t
                                print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                                counter = 0
                        
                        else:
                            pickBerryState = PickBerryState.APPROACH_BERRY
                            t_0 = t
                            print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                            counter = 0
                
                # Approach Berry
                if pickBerryState == PickBerryState.APPROACH_BERRY:
                    berryDist = 1
                    prcsn = 0.02
                    
                    if t-t_0 > 1 and strawberryDetector.z != 0.0:
                        myRobot.followItem( strawberryDetector.x, strawberryDetector.z, maxPickingDistance, prcsn )
                        #print('\nstrbry.x = {}, strbry.y = {}, strbry.z = {}'.format(strawberryDetector.x,strawberryDetector.y,strawberryDetector.z))
                        counter = 0
                    
                    elif t-t_0 > 1:
                        myRobot.stop()
                        counter = counter + 1
                        if counter >= 5:
                            counter = 0
                            pickBerryState = PickBerryState.EXIT
                            t_0 = t
                            print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                    if t-t_0 > 1:
                        if strawberryDetector.z <= maxPickingDistance and strawberryDetector.z > (myRobot.L_0z + myRobot.L_1 + myRobot.L_2) and abs(strawberryDetector.x) < prcsn and strawberryDetector.z != 0.0:
                            # if the strawberry is within picking position
                            myRobot.stop()
                            t_0 = t
                            theta0_0 = myRobot.servoAngle0_Offset - myRobot.servoAngle1
                            theta1_0 = theta0_0 + myRobot.servoAngle1_Offset - myRobot.servoAngle0
                            
                            theta0 = 180/np.pi * np.arcsin( 1/myRobot.L_1 * ( strawberryDetector.y + myRobot.L_2 - myRobot.L_0y ) )
                            
                            # Saturation for theta0:
                            if theta0 != theta0: # if theta0 == nan
                                theta0 = 0
                            if theta0 > myRobot.servoAngle0_Offset:
                                theta0 = myRobot.servoAngle0_Offset
                            elif theta0 < myRobot.servoAngle0_Offset-180:
                                theta0 = myRobot.servoAngle0_Offset-180
                            
                            theta1 = theta0-(180-myRobot.servoAngle1_Offset)
                            
                            # Saturation for theta1:
                            if theta1 < -(180-myRobot.servoAngle1_Offset):
                                theta1 = -(180-myRobot.servoAngle1_Offset)
                            
                            str_z = strawberryDetector.z
                            counter = 0

                            pickBerryState = PickBerryState.POSITION_ROBOT_ARM
                            t_0 = t
                            print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                        elif strawberryDetector.z < minPickingDistance and strawberryDetector.z != 0.0:
                            myRobot.backUp()

                if pickBerryState == PickBerryState.POSITION_ROBOT_ARM:
                    myRobot.moveRobotArm(theta0_0, 0, theta0, 0, theta1_0, 0, theta1, 0, T_arm, (t - t_0))
                    
                    if t-t_0 > T_arm:
                        theta0_0 = theta0
                        theta1_0 = theta1
                        gripperZ = myRobot.L_0z + myRobot.L_1*np.cos(theta0*np.pi/180) + myRobot.L_2*np.cos(theta1*np.pi/180)
                        dz = str_z - gripperZ
                        print('{}, {}'.format(dz, strawberryDetector.y))
                        t_moveToPickingPosition = 0.5 + dz# + strawberryDetector.y#*dz/0.05#0.25*dz/0.1
                        #print('\nt_moveToPickingPosition = {}'.format(t_moveToPickingPosition))
                    
                        pickBerryState = PickBerryState.MOVE_TO_PICKING_POSITION
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                if pickBerryState == PickBerryState.MOVE_TO_PICKING_POSITION:
                    myRobot.moveForward()

                    if t - t_0 > t_moveToPickingPosition:
                        myRobot.stop()
                        
                        theta0_0 = theta0
                        theta1_0 = theta1
                        theta1 = theta0 + 60
                    
                        pickBerryState = PickBerryState.PICK
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                if pickBerryState == PickBerryState.PICK:
                    myRobot.moveRobotArm(theta0_0, 0, theta0, 0, theta1_0, 0, theta1, 0, T_gripper, (t - t_0))
                    
                    if t-t_0 > T_gripper:
                        pickBerryState = PickBerryState.BACK_UP
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                if pickBerryState == PickBerryState.BACK_UP:
                    myRobot.backUp()

                    if t - t_0 > t_backUp:
                        myRobot.stop()
                    
                        theta0_0 = theta0
                        theta1_0 = theta1
                        theta0 = myRobot.servoAngle0_Offset
                        theta1 = theta1_0 + (theta0-theta0_0)
                    
                        pickBerryState = PickBerryState.POSITION_BERRY
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
  
                if pickBerryState == PickBerryState.POSITION_BERRY:
                    myRobot.moveRobotArm(theta0_0, 0, theta0, 0, theta1_0, 0, theta1, 0, T_arm, (t - t_0))                
                    
                    if t-t_0 > T_arm:
                        theta0_0 = theta0
                        theta1_0 = theta1
                        theta1 = theta0
                    
                        pickBerryState = PickBerryState.RELEASE_BERRY
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                if pickBerryState == PickBerryState.RELEASE_BERRY:
                    myRobot.moveRobotArm(theta0_0, 0, theta0, 0, theta1_0, 0, theta1, 0, T_gripper, (t - t_0))                
                    
                    if t-t_0 > T_gripper:
                        pickBerryState = PickBerryState.EXIT
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

                if pickBerryState == PickBerryState.EXIT:
                    myRobot.turnLeft()
                    
                    if t - t_0 > t_turnLeft:
                        myRobot.stop()
                    
                        pickBerryState = PickBerryState.INIT
                        mainState = MainState.INIT
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

            if mainState == MainState.CHANGE_SIDE and changeSideState == ChangeSideState.INIT:
                #theta0_0, theta1_0 = myRobot.getCurrentServoAngles()
                #myRobot.moveRobotArm(theta0_0, 0, 45, 0, theta1_0, 0, 45, 0, T_arm, (t - t_0))
                
                if t - t_0 < moveForwardTime(0.25):
                    myRobot.moveForward()
                else:
                    myRobot.stop()
                
                #if t - t_0 > T_arm:
                    myRobot.stop()
                    ID2_counter = 0
                    changeSideState = ChangeSideState.TURN_RIGHT
                    t_0 = t
                    print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
            
            if mainState == MainState.CHANGE_SIDE and changeSideState == ChangeSideState.TURN_RIGHT:
                #myRobot.turnRight()
                myRobot.leftWheelSpeed = 322
                myRobot.rightWheelSpeed = myRobot.leftWheelSpeed
                if arucoID == 2:#t - t_0 > t_turnRight/2:
                    ID2_counter = ID2_counter + 1
                    if ID2_counter >= 2:
                        changeSideState = ChangeSideState.MOVE_FORWARD
                        t_0 = t
                        print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                else:
                    ID2_counter = 0
            
            if mainState == MainState.CHANGE_SIDE and changeSideState == ChangeSideState.MOVE_FORWARD:
                myRobot.followItem(arucoX, arucoZ, 0.25, arucoPrcsn)
                
                if arucoZ < 0.26 and arucoZ != 0:#t - t_0 > t_moveToNextBerry:
                    changeSideState = ChangeSideState.EXIT
                    t_0 = t
                    print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))

            if mainState == MainState.CHANGE_SIDE and changeSideState == ChangeSideState.EXIT:
                myRobot.turnRight()
                
                if t - t_0 > t_turnRight:
                    changeSideState = ChangeSideState.INIT
                    mainState = MainState.INIT
                    t_0 = t
                    print('\n{}, {}, {}, at time {}'.format(mainState, pickBerryState, changeSideState,t))
                    #arucoZ = 1

            if mainState == MainState.FINISH:
                myRobot.stop()


            #print('left: {}, right: {}, ID: {}'.format(myRobot.leftWheelSpeed,myRobot.rightWheelSpeed, arucoID))

            myRobot.setServoMotors()
            #arucoID = 0

            # Sleep remaining time
            rate.sleep()
            t = t + dt
        
    except rospy.ROSInterruptException:
        pass